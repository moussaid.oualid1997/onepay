package com.payment.demo.service.impl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.payment.demo.dto.CommandLineDto;
import com.payment.demo.dto.ResponseDto;
import com.payment.demo.exception.DataException;
import com.payment.demo.model.CommandLine;
import com.payment.demo.repository.CommandLineRepository;
import com.payment.demo.repository.TransactionRepository;
import com.payment.demo.service.CommandLineService;

@Service
public class CommandLineServiceImpl implements CommandLineService {

	@Autowired
	CommandLineRepository repository;
	@Autowired
	TransactionRepository transactionRepository;

	@Override
	public CommandLine save(CommandLine commandLine) {
		if (commandLine.getCreatedAt() == null)
			commandLine.setCreatedAt(new Date());
		return repository.save(commandLine);
	}

	@Override
	public CommandLineDto getById(Long id) {
		CommandLine commandLine = repository.getById(id);
		return new CommandLineDto(commandLine);
	}

	@Override
	public ResponseDto<CommandLineDto, CommandLine> getAll(int page, int size) throws DataException {
		// TODO Auto-generated method stub
		return null;
	}

}
