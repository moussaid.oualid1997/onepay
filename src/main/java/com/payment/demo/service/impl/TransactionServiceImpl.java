package com.payment.demo.service.impl;

import static com.payment.demo.utils.AppConstants.CREATED_AT;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.payment.demo.dto.ResponseDto;
import com.payment.demo.dto.TransactionDto;
import com.payment.demo.dto.input.TransactionInputDto;
import com.payment.demo.enums.PaymentTypes;
import com.payment.demo.enums.Status;
import com.payment.demo.exception.DataException;
import com.payment.demo.model.CommandLine;
import com.payment.demo.model.Transaction;
import com.payment.demo.repository.CommandLineRepository;
import com.payment.demo.repository.TransactionRepository;
import com.payment.demo.service.TransactionService;
import com.payment.demo.utils.AppUtils;

@Service
public class TransactionServiceImpl implements TransactionService {

	@Autowired
	TransactionRepository transactionRepository;

	@Autowired
	CommandLineRepository commandLineRepository;

	@Override
	public Transaction save(Transaction transaction) {
		if (transaction.getCreatedAt() == null)
			transaction.setCreatedAt(new Date());
		return transactionRepository.save(transaction);
	}

	@Override
	public TransactionDto getById(Long id) {
		Transaction transaction = transactionRepository.getById(id);
		return new TransactionDto(transaction);
	}

	@Override
	public ResponseDto<TransactionDto, Transaction> getAll(int page, int size) throws DataException {
		AppUtils.validatePageNumberAndSize(page, size);

		Pageable pageable = PageRequest.of(page, size, Sort.Direction.DESC, CREATED_AT);
		Page<Transaction> transactionsPaged = transactionRepository.findAll(pageable);

		List<TransactionDto> transactionDtos = new ArrayList<>(transactionsPaged.getContent().size());
		for (Transaction transaction : transactionsPaged.getContent()) {
			transactionDtos.add(new TransactionDto(transaction));
		}

		if (transactionsPaged.getNumberOfElements() == 0) {
			return new ResponseDto<TransactionDto, Transaction>(Collections.emptyList(), transactionsPaged);
		}
		return new ResponseDto<TransactionDto, Transaction>(transactionDtos, transactionsPaged);
	}

	@Override
	@Transactional
	public TransactionDto addTransaction(TransactionInputDto inputDto) {
		if (PaymentTypes.typeOf(inputDto.getPaymentType()) != null) {
			Transaction transaction = new Transaction();
			transaction.setPaymentType(inputDto.getPaymentType());
			transaction.setAmount(inputDto.getAmount());
			Transaction savedTransaction = save(transaction);
			if (inputDto.getCommandLines() != null && inputDto.getCommandLines().size() > 0) {
				List<CommandLine> orders = new ArrayList<CommandLine>();
				inputDto.getCommandLines().forEach(inputDtoOrder -> {
					CommandLine commandLine = CommandLine.builder().createdAt(new Date())
							.price(inputDtoOrder.getPrice()).productName(inputDtoOrder.getProductName())
							.quantity(inputDtoOrder.getQuantity()).build();
					commandLine.setTransaction(savedTransaction);
					orders.add(commandLine);
					commandLineRepository.save(commandLine);
				});
				transaction.setCommandLines(orders);
			}
			return new TransactionDto(transaction);
		} else {
			return null;
		}
	}

	@Override
	@Transactional
	public TransactionDto updateTransaction(TransactionInputDto inputDto, Long id) {

		Transaction transaction = transactionRepository.getById(id);
		if (transaction != null && PaymentTypes.typeOf(inputDto.getPaymentType()) != null
				&& Status.typeOf(inputDto.getStatus()) != null) {
			transaction.setStatus(inputDto.getStatus());
			transaction.setPaymentType(inputDto.getPaymentType());
			Transaction updatedTransaction = save(transaction);
			return new TransactionDto(updatedTransaction);
		} else {
			return null;
		}
	}

	@Override
	public TransactionDto deleteTransaction(Long id) {
		Transaction transactionToDelete = transactionRepository.getById(id);
		if (transactionToDelete == null)
			return null;
		transactionRepository.delete(transactionToDelete);
		transactionToDelete.setCommandLines(null);
		return new TransactionDto(transactionToDelete);
	}
}
