package com.payment.demo.service;

import com.payment.demo.dto.AbstractDto;
import com.payment.demo.dto.ResponseDto;
import com.payment.demo.exception.DataException;
import com.payment.demo.model.AbstractModel;

public interface BaseService<T extends AbstractModel, M extends AbstractDto<T>> {

	public T save(T t);

	public M getById(Long id);

	public ResponseDto<M, T> getAll(int page, int size) throws DataException;
}
