package com.payment.demo.service;

import com.payment.demo.dto.TransactionDto;
import com.payment.demo.dto.input.TransactionInputDto;
import com.payment.demo.model.Transaction;

public interface TransactionService extends BaseService<Transaction, TransactionDto> {

	TransactionDto addTransaction(TransactionInputDto transactionInputDto);

	TransactionDto updateTransaction(TransactionInputDto inputDto, Long id);

	TransactionDto deleteTransaction(Long id);

}