package com.payment.demo.service;

import com.payment.demo.dto.CommandLineDto;
import com.payment.demo.model.CommandLine;

public interface CommandLineService extends BaseService<CommandLine, CommandLineDto> {

}
