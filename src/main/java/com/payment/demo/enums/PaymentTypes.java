package com.payment.demo.enums;

public enum PaymentTypes {

	CREDIT_CARD("CREDIT_CARD"), GIFT_CARD("GIFT_CARD"), PAYPAL("PAYPAL");

	PaymentTypes(String type) {
		this.type = type;
	}

	public String type;

	public static PaymentTypes typeOf(String type) {
		PaymentTypes[] values = PaymentTypes.values();
		for (int i = 0; i < values.length; i++) {
			if (values[i].type.equals(type)) {
				return values[i];
			}
		}
		return null;
	}
}
