package com.payment.demo.model;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.payment.demo.enums.Status;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;

@Entity
@ToString
@AllArgsConstructor
@Getter
@Setter
@Builder
public class Transaction extends AbstractModel {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@NonNull
	private Date createdAt;
	private String paymentType;
	private String status;
	private BigDecimal amount;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "transaction", orphanRemoval = false)
	private List<CommandLine> commandLines;

	public Transaction() {
		this.status = Status.NEW.status;
	}

	public void setStatus(String newStatus) {
		if (!Status.CAPTURED.status.equals(this.status)) {
			if (Status.CAPTURED.status.equals(newStatus)) {
				if (Status.AUTHORIZED.status.equals(this.status))
					this.status = newStatus;
			} else {
				this.status = newStatus;
			}
		}
	}
}
