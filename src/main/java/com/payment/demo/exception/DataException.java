package com.payment.demo.exception;

import org.springframework.http.HttpStatus;

public class DataException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final HttpStatus status;
	private final String message;

	public DataException(HttpStatus status, String message) {
		super();
		this.status = status;
		this.message = message;
	}

	public DataException(HttpStatus status, String message, Throwable exception) {
		super(exception);
		this.status = status;
		this.message = message;
	}

	public HttpStatus getStatus() {
		return status;
	}

	@Override
	public String getMessage() {
		return message;
	}

}