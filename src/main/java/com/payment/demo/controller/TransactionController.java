package com.payment.demo.controller;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Valid;
import javax.validation.Validation;
import javax.validation.ValidatorFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.payment.demo.dto.ResponseDto;
import com.payment.demo.dto.TransactionDto;
import com.payment.demo.dto.input.TransactionInputDto;
import com.payment.demo.model.Transaction;
import com.payment.demo.service.TransactionService;
import com.payment.demo.utils.AppConstants;

@RestController
@RequestMapping("/api/transactions")
public class TransactionController {

	@Autowired
	private TransactionService service;

	private ValidatorFactory factory = Validation.buildDefaultValidatorFactory();

	@GetMapping
	public ResponseDto<TransactionDto, Transaction> getPaged(
			@RequestParam(name = "page", required = false, defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) Integer page,
			@RequestParam(name = "size", required = false, defaultValue = AppConstants.DEFAULT_PAGE_SIZE) Integer size) {
		return service.getAll(page, size);
	}

	@GetMapping("/{id}")
	public ResponseEntity<TransactionDto> getOne(@PathVariable(name = "id") Long id) {
		TransactionDto dto = service.getById(id);

		return new ResponseEntity<>(dto, HttpStatus.OK);
	}

	@PostMapping
	public ResponseEntity<TransactionDto> addTransaction(@Valid @RequestBody TransactionInputDto transactionInputDto) {
		Set<ConstraintViolation<TransactionInputDto>> violations = factory.getValidator().validate(transactionInputDto);

		if (violations.size() > 0) {
			String message = violations.stream().map(violation -> violation.getMessage()).reduce("", String::concat);
			TransactionDto outDto = new TransactionDto();
			outDto.setMessage(message);
			return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(outDto);
		} else {
			TransactionDto added = service.addTransaction(transactionInputDto);
			if (added != null)
				return new ResponseEntity<>(added, HttpStatus.CREATED);
			else
				return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);
		}
	}

	@PutMapping("/{id}")
	public ResponseEntity<TransactionDto> updateTransaction(@PathVariable(name = "id") Long id,
			@Valid @RequestBody TransactionInputDto transactionInputDto) {
		Set<ConstraintViolation<TransactionInputDto>> violations = factory.getValidator().validate(transactionInputDto);

		if (violations.size() > 0) {
			String message = violations.stream().map(violation -> violation.getMessage()).reduce("", String::concat);
			TransactionDto outDto = new TransactionDto();
			outDto.setMessage(message);
			return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(outDto);
		} else {
			TransactionDto updated = service.updateTransaction(transactionInputDto, id);
			if (updated != null)
				return new ResponseEntity<>(updated, HttpStatus.CREATED);
			else
				return new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
		}
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<TransactionDto> deleteTransaction(@PathVariable(name = "id") Long id) {
		TransactionDto deleted = service.deleteTransaction(id);
		if (deleted != null)
			return new ResponseEntity<>(deleted, HttpStatus.OK);
		else
			return new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
	}

}