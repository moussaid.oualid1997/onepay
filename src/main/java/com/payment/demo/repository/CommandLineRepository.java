package com.payment.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.payment.demo.model.CommandLine;

@Repository
public interface CommandLineRepository extends JpaRepository<CommandLine, Long> {
}
