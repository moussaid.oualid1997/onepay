package com.payment.demo.dto;

import java.util.Collections;
import java.util.List;

import org.springframework.data.domain.Page;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.payment.demo.model.AbstractModel;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResponseDto<T extends AbstractDto, M extends AbstractModel> {
	private List<T> content;
	private int page;
	private int size;
	private long totalElements;
	private int totalPages;
	private boolean isLast;
	private String message;

	public ResponseDto(List<T> content, Page<M> pageResult) {
		setContent(content);
		this.page = pageResult.getNumber();
		this.size = pageResult.getSize();
		this.totalElements = pageResult.getTotalElements();
		this.totalPages = pageResult.getTotalPages();
		this.isLast = pageResult.isLast();
	}

	public List<T> getContent() {
		return content;
	}

	public final void setContent(List<T> content) {
		if (content == null) {
			this.content = null;
		} else {
			this.content = Collections.unmodifiableList(content);
		}
	}

}