package com.payment.demo.dto;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.payment.demo.model.Transaction;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TransactionDto extends AbstractDto<Transaction> {

	private Long id;
	private BigDecimal amount;
	private String paymentType;
	private String status;
	private List<CommandLineDto> commandLines;

	public TransactionDto(Transaction model) {
		this.id = model.getId();
		this.paymentType = model.getPaymentType();
		this.status = model.getStatus();
		if (model.getCommandLines() != null) {
			this.commandLines = new ArrayList<CommandLineDto>();
			model.getCommandLines().forEach(order -> {
				this.commandLines.add(new CommandLineDto(order));
			});
		}
		this.amount = model.getAmount();
	}
}
