package com.payment.demo.dto.input;

import java.math.BigDecimal;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class CommandLineInputDto {
	@NotBlank(message = "Product Name is required")
	private String productName;
	@NotNull(message = "Quantity is required")
	@PositiveOrZero(message = "Invalid Quantity (< 0)")
	private Integer quantity;
	@NotNull(message = "Price is required")
	@PositiveOrZero(message = "Invalid Price (< 0)")
	private BigDecimal price;
}
