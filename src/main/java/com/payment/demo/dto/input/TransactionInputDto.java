package com.payment.demo.dto.input;

import java.math.BigDecimal;
import java.util.List;

import javax.validation.constraints.NotBlank;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class TransactionInputDto {

	@NotBlank(message = "Payment Type is required")
	private String paymentType;
	private String status;
	private BigDecimal amount;
	private List<CommandLineInputDto> commandLines;

}
