package com.payment.demo.dto;

import com.payment.demo.model.AbstractModel;

import lombok.Getter;
import lombok.Setter;

public abstract class AbstractDto<T extends AbstractModel> {
	@Setter
	@Getter
	String message;
}
