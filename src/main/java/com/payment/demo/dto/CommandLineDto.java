package com.payment.demo.dto;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.payment.demo.model.CommandLine;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CommandLineDto extends AbstractDto<CommandLine> {
	private Long id;
	private String productName;
	private int quantity;
	private BigDecimal price;

	public CommandLineDto(CommandLine model) {
		this.id = model.getId();
		this.price = model.getPrice();
		this.productName = model.getProductName();
		this.quantity = model.getQuantity();
	}
}
