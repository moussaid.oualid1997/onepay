package com.payment.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PaymentDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(PaymentDemoApplication.class, args);
	}

//	@Bean
//	CommandLineRunner start(TransactionService transactionRepository, CommandLineService commandLineService) {
//		return args -> {
//			Transaction tr1 = Transaction.builder().status(Status.NEW.status).paymentType(PaymentTypes.PAYPAL.type)
//					.amount(BigDecimal.valueOf(66.6)).build();
//			Transaction tr2 = Transaction.builder().status(Status.NEW.status).paymentType(PaymentTypes.PAYPAL.type)
//					.amount(BigDecimal.valueOf(77.66)).build();
//
//			CommandLine cmd = CommandLine.builder().transaction(transactionRepository.save(tr1))
//					.price(BigDecimal.valueOf(10)).quantity(5).productName("pr1").build();
//			commandLineService.save(cmd);
//			CommandLine cmd2 = CommandLine.builder().transaction(transactionRepository.save(tr2))
//					.price(BigDecimal.valueOf(6)).quantity(2).productName("pr2").build();
//			commandLineService.save(cmd2);
//
//			transactionRepository.getAll().forEach(tr -> {
//				System.out.println("tr " + tr.toString());
//			});
//			commandLineService.getAll().forEach(cmd3 -> System.out.println("cmd " + cmd3.toString()));
//
//		};
//	}
}
